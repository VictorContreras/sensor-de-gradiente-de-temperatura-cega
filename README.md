# Sensor de gradiente de temperatura CEGA

Sensor de gradiente de temperatura para utilizar en el monitoreo de manera inalámbrica laderas de volcanes en el sur de Chile.
Desarrollado por Fablab U. de Chile para el Centro de excelencia geotérmica.

## ¿Cómo está construido?

La sonda consta con ocho nodos  de aluminio en los cuales por dentro están montados los sensores de temperatura (modelo del sensor), entre ellos están conectados con una tubería de PPR resistente a altas temperaturas.

<img src="/imagenes/sonda_despiece.JPG" width="285"> <img src="/imagenes/IMG_0757.JPG" width="285"> <img src="/imagenes/IMG_0750.JPG" width="285">

El arreglo de sensores de la sonda se conecta a una CPU, esta cumple la función de registrar los datos de lectura en una memoria física al mismo tiempo publica los datos a través de internet (poner como es que se comunica la sonda).

<img src="/imagenes/IMG_0049.JPG" width="285"> <img src="/imagenes/IMG_0051.JPG" width="285"> <img src="/imagenes/IMG_0056.JPG" width="285">

A dos metros de altura se posicionan el panel solar y la antena de comunicación, el panel nos permite una independencia energética a la sonda que se encuentra en una zona de alta montaña.

<img src="/imagenes_instalacion_de_prueba/diagrama_antena.jpg" width="285"> <img src="/imagenes_instalacion_de_prueba/IMG_0141.JPG" width="285"> <img src="/imagenes_instalacion_de_prueba/IMG_0142.JPG" width="285">
